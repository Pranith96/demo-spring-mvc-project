package com.demo.springmvc.model;

public class Employee {

	private Integer id;
	private String name;
	private String mobileNumber;
	private String company;
	private String password;

	public Employee(Integer id, String name, String mobileNumber, String company, String password) {
		this.id = id;
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.company = company;
		this.password = password;
	}

	public Employee() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", mobileNumber=" + mobileNumber + ", company=" + company
				+ ", password=" + password + "]";
	}

}
