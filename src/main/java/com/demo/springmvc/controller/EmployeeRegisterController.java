package com.demo.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.springmvc.model.Employee;

@Controller
@RequestMapping("/api")
public class EmployeeRegisterController {

	@ResponseBody
	@RequestMapping(value = "/data", method = RequestMethod.GET)
	public Employee employee() {
		Employee emp = new Employee();
		emp.setId(1);
		emp.setName("name");
		emp.setMobileNumber("1234567890");
		emp.setCompany("abc");
		emp.setPassword("123456789");
		return emp;
	}
}
